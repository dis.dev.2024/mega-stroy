"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import maskInput from './forms/input-mask.js'; // Маска ввода для форм
import mobileNav from './components/mobile-nav.js';  // Мобильное меню
import collapse from './components/collapse.js'; // Сворачиваемые блоки
import tabs from './components/tabs.js'; // Tabs
import Spoilers from "./components/spoilers.js";
import Dropdown from "./components/dropdown.js";
import { Fancybox } from "@fancyapps/ui"; // Fancybox modal gallery

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// Плавный скролл
usefulFunctions.SmoothScroll('[data-anchor]')

// Вкладки (tabs)
tabs();

// Сворачиваемые блоки
collapse();

// Маска для ввода номера телефона
maskInput('input[name="phone"]');


// Меню для мобильной версии
mobileNav();

// Spoilers
Spoilers();

// Dropdown
Dropdown();

const Media = () => {
    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-media-play]')) {

            const videoEl = event.target.closest('[data-media]').querySelector('[data-media-video]')

            event.target.closest('[data-media]').classList.toggle('media--play')
            if (videoEl.paused) {
                videoEl.play();
            } else {
                videoEl.pause();
            }
        }
    })
    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-media-mute]')) {
            event.target.closest('[data-media-mute]').classList.toggle('muted')
            const videoEl = event.target.closest('[data-media]').querySelector('[data-media-video]')
            if (videoEl.muted) {
                videoEl.muted = true
            }
            else {
                videoEl.muted = false
            }
        }
    })
}

Media()

// Modal Fancybox
Fancybox.bind("[data-fancybox]", {
    autoFocus: false
});

// Sliders
import "./components/sliders.js";
